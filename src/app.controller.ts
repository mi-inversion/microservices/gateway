import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { AppService } from './app.service';

import { UserDto } from './dto/user.dto';
import { InvestPerformanceInputsDto } from './dto/invest-performance-inputs.dto';
import { InvestPerformanceResultDto } from './dto/invest-performance-result-dto';
import { SuccessResponseDto } from './dto/success-response.dto';
import { LoginResponse } from './dto/login-response.dto';
import { PlanDto } from './dto/plan.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/login')
  login(@Body() user: UserDto): Promise<LoginResponse> {
    return this.appService.login(user.username, user.password);
  }

  @Post('/sign-up')
  signUp(@Body() user: UserDto): Promise<SuccessResponseDto> {
    return this.appService.signUp(user.username, user.password);
  }

  // PLANS

  @Post('/plans')
  createPlan(@Body() plan: PlanDto): Promise<PlanDto> {
    return this.appService.createPlan(plan);
  }

  @Get('/plans')
  getPlans(): Promise<PlanDto[]> {
    return this.appService.getPlans();
  }

  @Put('/plans')
  updatePlan(@Body() plan: PlanDto): Promise<PlanDto> {
    return this.appService.updatePlan(plan);
  }

  @Delete('/plans')
  deletePlan(@Body() plan: PlanDto): Promise<SuccessResponseDto> {
    return this.appService.deletePlan(plan);
  }

  // INVEST PERFORMANCE

  @Get('/invest-performance')
  generateInvestPerformance(
    @Query() inputs: InvestPerformanceInputsDto,
  ): Promise<InvestPerformanceResultDto> {
    return this.appService.generateInvestPerformance(inputs);
  }
}
