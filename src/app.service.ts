import { Injectable, Inject } from '@nestjs/common';
import { LoginResponse } from './dto/login-response.dto';
import { ClientProxy } from '@nestjs/microservices';
import { PlanDto } from './dto/plan.dto';
import { InvestPerformanceInputsDto } from './dto/invest-performance-inputs.dto';

const DDD_SERVICE = 'ddd';

@Injectable()
export class AppService {
  constructor(@Inject('SERVICES') private readonly client: ClientProxy) {}

  async login(username: string, password: string): Promise<LoginResponse> {
    const pattern = { service: DDD_SERVICE, cmd: 'login' };
    return await this.client.send(pattern, { username, password }).toPromise();
  }

  async signUp(username: string, password: string) {
    const pattern = { service: DDD_SERVICE, cmd: 'signUp' };
    return await this.client.send(pattern, { username, password }).toPromise();
  }

  // PLANS

  async createPlan(plan: PlanDto) {
    const pattern = { service: DDD_SERVICE, cmd: 'createPlan' };
    return await this.client.send(pattern, plan).toPromise();
  }

  async getPlans() {
    const pattern = { service: DDD_SERVICE, cmd: 'getPlans' };
    return await this.client.send(pattern, {}).toPromise();
  }

  async updatePlan(plan: PlanDto) {
    const pattern = { service: DDD_SERVICE, cmd: 'updatePlan' };
    return await this.client.send(pattern, plan).toPromise();
  }

  async deletePlan(plan: PlanDto) {
    const pattern = { service: DDD_SERVICE, cmd: 'deletePlan' };
    return await this.client.send(pattern, plan).toPromise();
  }

  // Invest performance

  async generateInvestPerformance(inputs: InvestPerformanceInputsDto) {
    const pattern = { service: DDD_SERVICE, cmd: 'generateInvestPerformance' };
    return await this.client.send(pattern, inputs).toPromise();
  }
}
