export class PlanDto {
  id: string;
  name: string;
  minInvest: number;
  maxInvest: number;
  monthlyRate: number;
  planDuration: number;
  performanceDelivery: number;
}
